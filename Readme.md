# Demo Applications
Sample Django Demo Applications

## Applications

### Housing Insights
===================

API Based application built using Django Rest Framework.
Functionalities includes:

1. Get current house prices in different location, locations have been prepopulated.
2. Predict future house prices in different locations. Due to the current data available for just a few years, prediction may stay on a straight line for a while.

#### Stock Chatty
===================

A simple stock API built using GraphQL query language

Functionalities includes:

1. Get stock prices of a particular company based on a number of days.

## Requirement
To run this application install the requirements in the requirement.txt file.
```bash
    pip install -r requirements.txt
```

## Run
Once all the requirements have been installed, navigate to this folder in a command line or terminal and run the below commands.

#### Local
1. Run init.sh to initialize DB an structures data.
    ```bash
      sh init.sh
    ```
2. Start application
    ```bash
      gunicorn HousingInsights.wsgi:application --workers 4 --timeout 360
    ```
3. Load Prices data once serve is up and running
    ```bash
      python3 loadPrices.py
    ```

After running:

1. Swagger UI can be accessed via [http://127.0.0.1:8000/v1/swagger/ui](http://127.0.0.1:8000/v1/swagger/ui)
2. GraphQL can be accessed via [http://127.0.0.1:8000/stocks/graphene](http://127.0.0.1:8000/stocks/graphene)

#### Deploy on Heroku
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/presola/Insights)





