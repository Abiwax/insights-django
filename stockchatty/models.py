from django.db import models
import jsonfield

# Create your models here.

class StockModel(models.Model):
    volume = models.BigIntegerField(default=0.0)
    days = models.IntegerField(default=0)
    symbol = models.CharField(max_length=100)
    high = models.FloatField(default=0.0)
    low = models.FloatField(default=0.0)
    date = models.CharField(default="0", max_length=100)
    open = models.FloatField(default=0.0)
    close = models.FloatField(default=0.0)

class SymbolStockModel(models.Model):
    Close = jsonfield.JSONField(default=[])
    Open = jsonfield.JSONField(default=[])
    Dates = jsonfield.JSONField(default=[])
    Symbol = models.CharField(max_length=100)
