from django.db import models

from graphene_django import DjangoObjectType
import graphene
from .models import SymbolStockModel, StockModel
# Create your models here.
from .views import get_data
from .utils import ratelimit
from django.http import JsonResponse

class Stocks(DjangoObjectType):
    class Meta:
        model = SymbolStockModel
        # filter_fields = ["symbol", "no_of_days"]

class StocksSingle(DjangoObjectType):
    class Meta:
        model = StockModel
        # filter_fields = ["symbol", "no_of_days"]

class Query(graphene.ObjectType):

    stocks = graphene.List(Stocks, symbol=graphene.String(), days=graphene.Int())
    stocks_expanded = graphene.List(StocksSingle, symbol=graphene.String(), days=graphene.Int())

    @ratelimit(key="ip", rate="3/d", block=True)
    def resolve_stocks(self, info, **kwargs):
        return get_data(kwargs)

    @ratelimit(key="ip", rate="3/d", block=True)
    def resolve_stocks_expanded(self, info, **kwargs):
        return get_data(kwargs, expanded=True)

schema = graphene.Schema(query=Query)
