#!/usr/bin/env python

from rest_framework import serializers
from .models import SymbolStockModel

class SymbolStockModelSerializer(serializers.ModelSerializer):
    Dates = serializers.SerializerMethodField()
    Open = serializers.SerializerMethodField()
    Close = serializers.SerializerMethodField()


    class Meta:
        model = SymbolStockModel
        fields = ('Symbol', 'Dates', 'Open', 'Close', )

    def get_Dates(self, json):
        return json.Dates

    def get_Open(self, json):
        return json.Open

    def get_Close(self, json):
        return json.Close
