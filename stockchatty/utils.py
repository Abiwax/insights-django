from ratelimit.exceptions import Ratelimited
from ratelimit.utils import is_ratelimited
from functools import wraps
from ratelimit import ALL

def GQLRatelimitKey(group, request):
    return request.gql_rl_field

# Rate limit decorator
def ratelimit(group=None, key=None, rate=None, method=ALL, block=False):
    def decorator(fn):
        @wraps(fn)
        def _wrapped(root, info, **kw):
            request = info.context
            request.limited = getattr(request, "limited", False)

            new_key = key
            if key and key.startswith("gql:"):
                _key = key.split("gql:")[1]
                value = kw.get(_key, None)
                if not value:
                    raise ValueError(f"Cannot get key: {key}")
                request.gql_rl_field = value

                new_key = GQLRatelimitKey

            ratelimited = is_ratelimited(
                request=request,
                group=group,
                fn=fn,
                key=new_key,
                rate=rate,
                method=method,
                increment=True,
            )
            if ratelimited and block:
                raise Ratelimited('You have exceeded your number of requests for a day (5/day).')
            return fn(root, info, **kw)

        return _wrapped

    return decorator
