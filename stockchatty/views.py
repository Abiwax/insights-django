from yahoo_finance_api2 import share
from .models import StockModel, SymbolStockModel
from datetime import datetime
import time
# Called from the GraphQL Query, function calls the retrieve function which uses the finance package
def get_data(kwargs, expanded=False):
    symbol = kwargs.get('symbol')
    days = kwargs.get('days')
    if expanded:
        return retrieve_stock(symbol, days)
    else:
        return retrieve(symbol, days)


# Calls the finance package to get data based on no of days and symbol of company
# returns list of SymbolStockModel Object
def retrieve(symbol, days):
    data = []
    try:
        split_symbol = symbol.split(',')
        for i in split_symbol:
            print("Getting data")
            print(i)
            new_symbol = i.strip()
            try:
                # pass stock symbol of company to Share class
                yahoo = share.Share(new_symbol)

                # get historical yahoo data based on the number of days
                result = yahoo.get_historical(share.PERIOD_TYPE_DAY, days, share.FREQUENCY_TYPE_DAY, 1)

                # clean data to return model object declaration
                if 'volume' in result:
                    symbol_res = {'Open': result['open'], 'Close': result['close'], 'Dates': result['timestamp'], 'Symbol': new_symbol}

                    data.append(SymbolStockModel(**symbol_res))
            except Exception as ex:
                print("Failed to retrieve single symbol data.")
                print(new_symbol)
                print(ex)
    except Exception as ex:
        print("Failed to retrieve data.")
        print(symbol)
        print(ex)

    return data

# Calls the finance package to get data based on no of days and symbol of company,
# returns list of StockModel Object
def retrieve_stock(symbol, days):
    data = []
    try:
        split_symbol = symbol.split(',')

        for i in split_symbol:
            print("Getting data")
            print(i)
            new_symbol = i.strip()
            try:
                # pass stock symbol of company to Share class
                yahoo = share.Share(new_symbol)

                # get historical yahoo data based on the number of days
                result = yahoo.get_historical(share.PERIOD_TYPE_DAY, days, share.FREQUENCY_TYPE_DAY, 1)
                # clean data to return model object declaration
                if 'volume' in result:
                    new_data = [
                        StockModel(**{'volume': info, 'symbol': new_symbol, 'high': result['high'][index], 'low': result['low'][index],
                                      'close': result['close'][index], 'open': result['open'][index],
                                      'date': datetime.utcfromtimestamp(int(result['timestamp'][index])/1000).strftime('%Y-%m-%d')}) for
                        index, info in enumerate(result['volume'])]
                    data.extend(new_data)
            except Exception as ex:
                print("Failed to retrieve data.")
                print(new_symbol)
                print(ex)
    except Exception as ex:
        print("Failed to retrieve data.")
        print(symbol)
        print(ex)

    return data
